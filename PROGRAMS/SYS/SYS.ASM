; ||           $AX            ||




	BITS 16

	ORG 7168

; ------------------------------
; ## FUNCTION CALLS ##

	boot_start		equ 0000h ; Generally not used, completely restarts the OS
	start			equ 0003h ; Generally not used, allows user to start a program one layer down
	print_string		equ 0006h ; Prints a zero terminated string to the screen
	read_file		equ 0009h ; Allows user to load a program from disk to RAM using a filename
	write_file		equ 000Ch ; Allows user to load a program from RAM to disk using a filename
	disk_manage		equ 000Fh ; Allows for custom use of the disk - USE WITH CARE!
	get_key_press		equ 0012h ; Waits for the user to press a key and returns it
	get_string_and_print	equ 0015h ; Gets a string of maximum length from the user and returns it
	string_compare		equ 0018h ; Compares two strings and tells the user if they are equal
	error			equ 001Bh ; Prints an OS error to the screen. If jumped to this will also exit
					  ;  the user from the program
	find_file		equ 001Eh ; Searches for a file in the file table and returns whether it was
					  ;  found or not
	write_allocation_table	equ 0021h ; Places a filename in a position that corresponds to a given sector
	print_char		equ 0024h ; Prints a single character to the screen
	remove_file		equ 0027h ; Allows for a given filename to deleted from the file allocation
					  ;  table. Note the contents remain until over-written

; ------------------------------
; ## MAIN CODE ##

main:
	add si, length_of_file + 1

.param:
	cmp byte [si], '-'
	jne print_info

	inc si

.test:
	cmp byte [si], 'h'
	je print_help

	cmp byte [si], 'v'
	je print_version

	jmp error

; ------------------------------
; ## MAIN CODE ##

print_info:
	int 11h			; Get BIOS equipment list
	mov bx, ax

	mov si, msg1
	call print_message

	mov al, bh
	and al, 11000000b
	shr al, 6
	call print_number

	mov si, msg2
	call print_message

	mov al, bh
	and al, 00010000b
	call print_answer

	mov si, msg3
	call print_message

	mov al, bh
	and al, 00001100b
	shr al, 2
	call print_number

	mov si, msg4
	call print_message

	mov al, bl
	and al, 11000000b
	shr al, 6
	inc al
	call print_number

	mov si, msg5
	call print_message

	mov al, bl
	and al, 00110000b
	call print_videomode

	mov si, msg6
	call print_message

	mov al, bl
	and al, 00000100b
	call print_answer

	mov si, msg7
	call print_message

	mov al, bl
	and al, 00000010b
	call print_answer

	mov si, msg8
	call print_message

	mov al, bl
	and al, 00000001b
	call print_answer

	ret

; ------------------------------
; ## SCREEN CALLS ##

print_message:
	push si
	mov si, newline
	call print_string
	pop si
	call print_string
	ret

print_number:
	add al, 48
	call print_char
	ret

print_answer:
	cmp al, 0
	je .print_no

	mov al, 'Y'
	call print_char

	ret

.print_no:
	mov al, 'N'
	call print_char

	ret

print_videomode:
	cmp al, 0
	jne .maybe_one

	mov si, zero
	call print_string

.maybe_one:
	cmp al, 00010000b
	jne .maybe_two

	mov si, one
	call print_string

.maybe_two:
	cmp al, 00100000b
	jne .maybe_three

	mov si, two
	call print_string

.maybe_three:
	cmp al, 00110000b
	jne .done

	mov si, three
	call print_string

.done:
	ret

; ------------------------------
; ## PARAM CALLS ##

print_help:
	mov si, help
	call print_string
	ret

print_version:
	mov si, version
	call print_string
	ret

; ------------------------------
; ## VARIABLES ##

version	db 10, 13, 'ver 1', 0
help	db 10, 13, '-h Help'
help2	db 10, 13, '-v Version', 0

file_table	equ 1024	; Location of File Table
length_of_file	equ 3		; The length of the Filename
num_of_files	equ 1440	; Number of files in table

newline	db 10, 13, 0
zero	db 'EGA/VGA/PGA/Other',0
one	db '40x25 CGA',0
two	db '80x25 CGA',0
three	db '80x25 mono',0

msg1	db 'No. parallel devices:', 0
msg2	db 'Game port installed:', 0
msg3	db 'No. serial devices:', 0
msg4	db 'No. floppy disk drives:', 0
msg5	db 'Initial video mode:', 0
msg6	db 'PS/2 mouse installed:', 0
msg7	db 'Math coprocessor installed:', 0
msg8	db 'Booted from floppy:', 0

; ## SIGNATURE ##

	times 510-($-$$)db 0	; Pad remainder of boot sector with zeros
	dw 02486h		; Boot signature (DO NOT CHANGE!)
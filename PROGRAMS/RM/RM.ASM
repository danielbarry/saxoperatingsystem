; ||           $AX            ||




	BITS 16

	ORG 7168

; ------------------------------
; ## FUNCTION CALLS ##

	boot_start		equ 0000h ; Generally not used, completely restarts the OS
	start			equ 0003h ; Generally not used, allows user to start a program one layer down
	print_string		equ 0006h ; Prints a zero terminated string to the screen
	read_file		equ 0009h ; Allows user to load a program from disk to RAM using a filename
	write_file		equ 000Ch ; Allows user to load a program from RAM to disk using a filename
	disk_manage		equ 000Fh ; Allows for custom use of the disk - USE WITH CARE!
	get_key_press		equ 0012h ; Waits for the user to press a key and returns it
	get_string_and_print	equ 0015h ; Gets a string of maximum length from the user and returns it
	string_compare		equ 0018h ; Compares two strings and tells the user if they are equal
	error			equ 001Bh ; Prints an OS error to the screen. If jumped to this will also exit
					  ;  the user from the program
	find_file		equ 001Eh ; Searches for a file in the file table and returns whether it was
					  ;  found or not
	write_allocation_table	equ 0021h ; Places a filename in a position that corresponds to a given sector
	print_char		equ 0024h ; Prints a single character to the screen
	remove_file		equ 0027h ; Allows for a given filename to deleted from the file allocation
					  ;  table. Note the contents remain until over-written

; ------------------------------
; ## MAIN CODE ##

main:
	add si, length_of_rm + 1

.param:
	cmp byte [si], '-'
	jne delete_file

	inc si

.test:
	cmp byte [si], 'h'
	je print_help

	cmp byte [si], 'v'
	je print_version

	jmp error

; ------------------------------
delete_file:
	cmp byte [si], 0
	je print_error

	mov ah, 0
check_name:
	cmp ah, filename_size
	je okay

	cmp byte [si], ' '	; Parameter reserved
	je filename_error

	cmp byte [si], '-'	; Parameter reserved
	je filename_error

	cmp byte [si], '/'	; Reserve for later folders
	je filename_error

	inc ah
	inc si

	jmp check_name

okay:
	sub si, filename_size

	mov bx, si
	call remove_file

	ret

filename_error:
	jmp print_error
	ret

; ------------------------------
print_error:
	call error
	ret

; ------------------------------
print_help:
	mov si, help
	call print_string

	ret

; ------------------------------
print_version:
	mov si, version
	call print_string

	ret

; ------------------------------
; ## VARIABLES ##

version	db 10, 13, 'ver 5', 0
newline	db 10, 13, 0
help	db 10, 13, '[file] Remove file'
help2	db 10, 13, '-h Help'
help3	db 10, 13, '-v Version', 0

file_table	equ 1024	; Location of File Table
filename_size	equ 4		
length_of_rm	equ 2		; The length of the Filename 'ls'
num_of_files	equ 1440	; Number of files in table

; ## SIGNATURE ##

	times 510-($-$$)db 0	; Pad remainder of boot sector with zeros
	dw 02486h		; Boot signature (DO NOT CHANGE!)

file:
; || $AX ||

BITS 16

; ------------------------------
; ## VARIABLES ##

text1 db 'Welcome to $AX, a light weight terminal based Operating System.', 13
text2 db 'Below is some basic information on progress made.', 13
text3 db '--------------------------------------------------------------------------------'
text4 db '-KERN VER=5 BUILD=034 : -TBLE VER=  BUILD=008 : -LS   VER=5 BUILD=011', 13
text5 db '-CAT  VER=5 BUILD=009 : -MK   VER=5 BUILD=008 : -EDIT VER=6 BUILD=026', 13
text6 db '-TEXT VER=  BUILD=006 : -RM   VER=5 BUILD=004 : -RUN  VER=1 BUILD=014', 13
text7 db '-CODE VER=  BUILD=001 : -SPCE VER=1 BUILD=011 : -SYS  VER=1 BUILD=002'

; ------------------------------
; ## SIGNATURE ##

times 512-($-$$)db 0 ; Pad remainder of boot sector with zeros
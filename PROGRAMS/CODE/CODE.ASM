; ||           $AX            ||




	BITS 16

; ------------------------------
; ## VARIABLES ##

text0	db 'b="password"', 13
text1	db 'c="exit"', 13
text2	db 'out "Type exit if you give up."', 13
text3	db 'out "Password>"', 13
text4	db 'in a', 13
text5	db 'if a=c', 13
text6	db 'go 13', 13
text7	db 'if a!b', 13
text8	db 'out "Incorrect!"', 13
text9	db 'if a!b', 13
text10	db 'go 02', 13
text11	db 'out "Correct :)"', 13
text12	db 'end', 13
text13	db 'out "You give up!"', 13
text14	db 'end'

; ## SIGNATURE ##

	times 512-($-$$)db 0	; Pad remainder of boot sector with zeros

buffer:
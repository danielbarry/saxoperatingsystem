# | $AX Make File |
#
# NOTE: This is Linux dependent and relies on bash.
# TODO: [ ] Build either a cross platform version of this or build it for many
#           different OS's.
#       [/] Fix the way in which we write floppy disk images (sectors are
#           logical/serial in nature)

# Clear the screen
#clear

# Get build version info
major=$(grep "major " KERNEL/KERN.ASM | cut -f 5 -d' ')
minor=$(grep "minor " KERNEL/KERN.ASM | cut -f 5 -d' ')
patch=$(grep "patch " KERNEL/KERN.ASM | cut -f 5 -d' ')
build=$(echo "${major}.${minor}.${patch}" | tr '\n' '\0')

# Title
echo "____________"
echo "____MAKE____"
echo "Build=$build"

# List of filenames with location to be compiled
# NOTE: Items added here should also be added to the .gitignore file so that
#       they don't get included in commits!
list=(
       KERNEL/KERN        #  1
       PROGRAMS/TBLE/TBLE # 12
       PROGRAMS/LS/LS     #  1
       PROGRAMS/CAT/CAT   #  1
       PROGRAMS/MK/MK     #  1
       PROGRAMS/EDIT/EDIT #  1
       PROGRAMS/TEXT/TEXT #  1
       SPACE/SPACE        # 18
       PROGRAMS/RM/RM     #  1
       PROGRAMS/RUN/RUN   #  1
       PROGRAMS/CODE/CODE #  1
       PROGRAMS/SPCE/SPCE #  1
       PROGRAMS/SYS/SYS   #  1
       PROGRAMS/TEST/TEST #  1
       PROGRAMS/GOL/GOL   #  1
       SPACE/SPACE_SMALL  #  1
     )
x=44                      # 44

# Output file
release="${build}_$(date '+%G-%m-%d_%H:%M:%S')"

# Iterate through list of files to be compiled
for var in "${list[@]}"
do
  cp FUNCTIONS.ASM $(dirname $var)/FUNCTIONS.ASM
  
  rm         ${var}
  nasm       ${var}.ASM
  ls -la     ${var}
  
  rm $(dirname $var)/FUNCTIONS.ASM
done

# Create image file
cat ${list[*]} >> RELEASES/${release}.IMA

# Fill space for rest of disk
x=$((1474560 - (512 * $x)))
while [ $x -gt 0 ]
do
  cat SPACE/SPACE_SMALL >> RELEASES/${release}.IMA
  x=$(($x - 512))
done

# Create a zip file with binaries, code, docs and image inside

# Singular copy
zip -9 RELEASES/${release}.ZIP RELEASES/${release}.IMA FUNCTIONS.ASM
# Recursive copy
zip -9 -r RELEASES/${release}.ZIP DOCS KERNEL PROGRAMS
